#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "otb.h"

int readFile(char* path, long* fileSize, void** fileContents) {
	FILE *fp = fopen(path, "rb");
	if (!fp) {
		perror("fopen");
		return -1;
	}

	int fseekStatus = fseek(fp, 0, SEEK_END);
	if (fseekStatus == -1) {
		fclose(fp);
		perror("fseek");
		return -1;
	}

	*fileSize = ftell(fp);
	/* printf("File has %ld bytes\n",  *fileSize); */

	fseekStatus = fseek(fp, 0, SEEK_SET);
	if (fseekStatus == -1) {
		fclose(fp);
		perror("fseek");
		return -1;
	}

	*fileContents = malloc(*fileSize);

	size_t ret = fread(*fileContents, 1, *fileSize, fp);
	if (ret != *fileSize) {
		fprintf(stderr, "fread() failed: %zu\n", ret);
		return -1;
	}

	int closeStatus = fclose(fp);
	if (closeStatus != 0) {
		perror("fclose");
		return -1;
	}

	return 0;
}

char* groupToString(ItemGroup group) {
	char* ret;
	switch (group) {
		case ITEM_GROUP_NONE:
			ret = "NONE";
		break;
		case ITEM_GROUP_GROUND:
			ret = "GROUND";
		break;
		case ITEM_GROUP_CONTAINER:
			ret = "CONTAINER";
		break;
		case ITEM_GROUP_WEAPON:
			ret = "WEAPON";
		break;
		case ITEM_GROUP_AMMUNITION:
			ret = "AMMUNITION";
		break;
		case ITEM_GROUP_ARMOR:
			ret = "ARMOR";
		break;
		case ITEM_GROUP_CHARGES:
			ret = "CHARGES";
		break;
		case ITEM_GROUP_TELEPORT:
			ret = "TELEPORT";
		break;
		case ITEM_GROUP_MAGICFIELD:
			ret = "MAGICFIELD";
		break;
		case ITEM_GROUP_WRITEABLE:
			ret = "WRITEABLE";
		break;
		case ITEM_GROUP_KEY:
			ret = "KEY";
		break;
		case ITEM_GROUP_SPLASH:
			ret = "SPLASH";
		break;
		case ITEM_GROUP_FLUID:
			ret = "FLUID";
		break;
		case ITEM_GROUP_DOOR:
			ret = "DOOR";
		break;
		case ITEM_GROUP_DEPRECATED:
			ret = "DEPRECATED";
		break;
		case ITEM_GROUP_LAST:
			ret = "LAST";
		break;
		default:
			ret = "";
	}
	return ret;
}

/**
 * hash - 16 byte input
 * str - 32 byte output
 */
void hashToString(char* hash, char* str) {
	char* table = "0123456789ABCDEF";
	for (int i = 0; i < 16; i++) {
		str[i*2] = table[hash[i]/16];
		str[i*2+1] = table[hash[i]%16];
	}
}

/**
 * Provide at least 512 available bytes to the ret variable
 * FIXME: buffer overflow
 */
void itemFlagsToString(ItemFlags flags, char* ret) {
	if (ITEM_FLAG_BLOCK_SOLID & flags) {
		strcat(ret,"\"BLOCK_SOLID\",");
	}
	if (ITEM_FLAG_BLOCK_PROJECTILE & flags) {
		strcat(ret,"\"BLOCK_PROJECTILE\",");
	}
	if (ITEM_FLAG_BLOCK_PATHFIND & flags) {
		strcat(ret,"\"BLOCK_PATHFIND\",");
	}
	if (ITEM_FLAG_HAS_HEIGHT & flags) {
		strcat(ret,"\"HAS_HEIGHT\",");
	}
	if (ITEM_FLAG_USABLE & flags) {
		strcat(ret,"\"USABLE\",");
	}
	if (ITEM_FLAG_PICKUPABLE & flags) {
		strcat(ret,"\"PICKUPABLE\",");
	}
	if (ITEM_FLAG_MOVABLE & flags) {
		strcat(ret,"\"MOVABLE\",");
	}
	if (ITEM_FLAG_STACKABLE & flags) {
		strcat(ret,"\"STACKABLE\",");
	}
	if (ITEM_FLAG_FLOORCHANGEDOWN & flags) {
		strcat(ret,"\"FLOORCHANGEDOWN\",");
	}
	if (ITEM_FLAG_FLOORCHANGENORTH & flags) {
		strcat(ret,"\"FLOORCHANGENORTH\",");
	}
	if (ITEM_FLAG_FLOORCHANGEEAST & flags) {
		strcat(ret,"\"FLOORCHANGEEAST\"");
	}
	if (ITEM_FLAG_FLOORCHANGEWEST & flags) {
		strcat(ret,"\"FLOORCHANGEWEST\",");
	}
	if (ITEM_FLAG_ALWAYSONTOP & flags) {
		strcat(ret,"\"ALWAYSONTOP\",");
	}
	if (ITEM_FLAG_READABLE & flags) {
		strcat(ret,"\"READABLE\",");
	}
	if (ITEM_FLAG_ROTABLE & flags) {
		strcat(ret,"\"ROTABLE\",");
	}
	if (ITEM_FLAG_HANGABLE & flags) {
		strcat(ret,"\"HANGABLE\",");
	}
	if (ITEM_FLAG_VERTICAL & flags) {
		strcat(ret,"\"VERTICAL\",");
	}
	if (ITEM_FLAG_HORIZONTAL & flags) {
		strcat(ret,"\"HORIZONTAL\",");
	}
	if (ITEM_FLAG_CANNOTDECAY & flags) {
		strcat(ret,"\"CANNOTDECAY\",");
	}
	if (ITEM_FLAG_ALLOWDISTREAD & flags) {
		strcat(ret,"\"ALLOWDISTREAD\",");
	}
	if (ITEM_FLAG_UNUSED & flags) {
		strcat(ret,"\"UNUSED\",");
	}
	if (ITEM_FLAG_CLIENTCHARGES & flags) {
		strcat(ret,"\"CLIENTCHARGES\",");
	}
	if (ITEM_FLAG_LOOKTHROUGH & flags) {
		strcat(ret,"\"LOOKTHROUGH\",");
	}
	if (ITEM_FLAG_ANIMATION & flags) {
		strcat(ret,"\"ANIMATION\",");
	}
	if (ITEM_FLAG_WALKSTACK & flags) {
		strcat(ret,"\"WALKSTACK\",");
	}
	size_t len = strlen(ret);
	if (len > 0) {
		ret[len-1] = 0;
	}
}

void onItemType(ItemType item) {
	char buf[512] = "";
	itemFlagsToString(item.flags, buf);
	int length = item.name.length;
	char hashString[33];
	hashToString(item.hash, hashString);
	hashString[32] = 0;
	printf(
		"{\"type\": \"item\", \"name\": \"%-*s\", \"group\": \"%s\", \"flags\": [%s], \"serverId\": %d, \"clientId\": %d, \"speed\": %d, \"light2\": {\"level\": %d, \"color\": %d}, \"topOrder\": %d, \"wareId\": %d, \"hash\": \"%s\", \"miniMapColor\": %d}\n", 
		length,
		item.name.ptr,
		groupToString(item.group),
		buf,
		item.serverId,
		item.clientId,
		item.speed,
		item.light2.level,
		item.light2.color,
		item.topOrder,
		item.wareId,
		hashString,
		item.miniMapColor
	);
}

void onOTBVersion(VersionInfo version) {
	printf(
		"{\"type\": \"version\", \"otb\":%d, \"client\":%d, \"build\":%d}\n",
		version.otb_version,
		version.client_version,
		version.build
	);
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("Usage: %s /path/to/file.otbm\n",argv[0]);
		return 1;
	}

	char* path = argv[1];

	long fileSize;
	void* fileContents;
	int fileStatus = readFile(argv[1], &fileSize, &fileContents);
	if (fileStatus != 0) {
		return fileStatus;
	}

	ParserOTB parser;
	parser.onItemType = &onItemType;
	parser.onOTBVersion = &onOTBVersion;
	parseOTB(parser, fileSize, fileContents);

	return 0;
}

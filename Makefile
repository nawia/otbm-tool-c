all:
	gcc -c -o main.o main.c
	mkdir bin && gcc main.o -lphobos2 -pthread -lotbm -obin/otbmtool-c
install:
	mkdir ${out}/bin
	install bin/otbmtool-c ${out}/bin/otbmtool-c
